@extends('frontend.common.template', ['notFoundPage' => true])

@section('content')

    <div class="not-found">
        <h1>Página não encontrada</h1>
    </div>

@endsection

