    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>
            <div class="right">
                @if($contato->linkedin || $contato->facebook)
                <div class="social">
                    @foreach(['linkedin', 'facebook'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>
                @endif
                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>
            <nav id="menu">
                @include('frontend.common.nav')
            </nav>
        </div>
    </header>
