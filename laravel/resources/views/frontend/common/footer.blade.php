    @unless(isset($notFoundPage))
    <div class="contato" id="contato">
        <div class="titulo-wrapper">
            <div class="center">
                <h2 class="titulo">Contato</h2>
            </div>
        </div>

        <div class="center">
            <div class="contato-informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="email">
                    <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                </p>
                <p class="endereco">{{ $contato->endereco }}</p>

                <h3>Fale conosco</h3>
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                    <div id="form-contato-response"></div>
                </form>
            </div>
        </div>

        <div class="contato-mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>
    @endunless

    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ $config->nome_do_site }} &middot; Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de Sites:</a>
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
