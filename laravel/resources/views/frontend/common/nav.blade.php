@if(Route::currentRouteName() == 'home')
<a href="#home" class="scroll-handle">Home</a>
<a href="#a-lobus" class="scroll-handle">A Lobus</a>
<div class="has-sub">
    <a href="#produtos" class="scroll-handle">Produtos</a>
    <a href="#" class="submenu-handle"></a>
</div>
<div class="submenu" style="display:none">
    @foreach($produtosLista as $slug => $titulo)
    <a href="{{ route('produto', $slug) }}" class="sub">{{ $titulo }} &laquo;</a>
    @endforeach
</div>
<div class="has-sub">
    <a href="#servicos" class="scroll-handle">Serviços</a>
    <a href="#" class="submenu-handle"></a>
</div>
<div class="submenu" style="display:none">
    @foreach($servicosLista as $id => $servico)
    <a href="#servicos" class="sub scroll-handle scroll-servicos" data-id="{{ $id }}">{{ $servico->titulo }} &laquo;</a>
    @endforeach
</div>
<a href="#contato" class="scroll-handle">Contato</a>
@else
<a href="{{ route('home') }}">Home</a>
<a href="{{ route('home').'#a-lobus' }}">A Lobus</a>
<div class="has-sub">
    <a href="{{ route('home').'#produtos' }}">Produtos</a>
    <a href="#" class="submenu-handle"></a>
</div>
<div class="submenu" style="display:none">
    @foreach($produtosLista as $slug => $titulo)
    <a href="{{ route('produto', $slug) }}" class="sub">{{ $titulo }} &laquo;</a>
    @endforeach
</div>
<div class="has-sub">
    <a href="{{ route('home').'#servicos' }}">Serviços</a>
    <a href="#" class="submenu-handle"></a>
</div>
<div class="submenu" style="display:none">
    @foreach($servicosLista as $id => $servico)
    <a href="{{ route('home', ['id' => $id]).'#servicos' }}" class="sub">{{ $servico->titulo }} &laquo;</a>
    @endforeach
</div>
<a href="{{ route('home').'#contato' }}">Contato</a>
@endif
