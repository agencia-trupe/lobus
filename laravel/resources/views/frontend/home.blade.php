@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
            @if($banner->link)
            <a href="{{ $banner->link }}" class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            @else
            <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            @endif
                <div class="center">
                    <div class="wrapper">
                        @if($banner->titulo)
                        <h3>{!! $banner->titulo !!}</h3>
                        @endif
                        @if($banner->subtitulo)
                        <h4>{!! $banner->subtitulo !!}</h4>
                        @endif
                        @if($banner->texto)
                        <p>{!! $banner->texto !!}</p>
                        @endif
                    </div>
                </div>
            @if($banner->link)
            </a>
            @else
            </div>
            @endif
        @endforeach
        <div class="cycle-pager"></div>
        <div class="setas">
            <div class="center">
                <a href="#" id="cycle-prev">&laquo;</a>
                <a href="#" id="cycle-next">&raquo;</a>
            </div>
        </div>
    </div>

    <div class="produtos" id="produtos">
        <div class="titulo-wrapper">
            <div class="center">
                <h2 class="titulo">Produtos</h2>
            </div>
        </div>

        <div class="arvore">
            <div class="center">
                @foreach($icones as $key => $icone)
                    @if($icone->link)
                    <a href="{{ $icone->link }}" class="icone icone-{{ ++$key }}">
                        <img src="{{ asset('assets/img/icones/'.$icone->imagem) }}" alt="">
                    </a>
                    @else
                    <div class="icone icone-{{ ++$key }}">
                        <img src="{{ asset('assets/img/icones/'.$icone->imagem) }}" alt="">
                    </div>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="center">
            <div class="produtos-thumbs">
                @foreach($produtos as $produto)
                <a href="{{ route('produto', $produto->slug) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="servicos" id="servicos">
        <div class="titulo-wrapper">
            <div class="center">
                <h2 class="titulo">Serviços</h2>
            </div>
        </div>

        <div class="center">
            <div class="servicos-slides">
                @foreach($servicos as $servico)
                <div class="slide">
                    <div class="titulo">
                        <img src="{{ asset('assets/img/servicos/'.$servico->icone) }}" alt="">
                        <h3>{{ $servico->titulo }}</h3>
                    </div>
                    <div class="texto">{!! $servico->texto !!}</div>
                </div>
                @endforeach

                <div class="cycle-pager"></div>
                <a href="#" class="servicos-seta" id="cycle-prev-s">&laquo;</a>
                <a href="#" class="servicos-seta" id="cycle-next-s">&raquo;</a>
            </div>
        </div>
    </div>

    <div class="a-lobus" id="a-lobus">
        <div class="titulo-wrapper">
            <div class="center">
                <h2 class="titulo">A Lobus</h2>
            </div>
        </div>

        <div class="center">
            <div class="texto">
                <img src="{{ asset('assets/img/layout/logo.png') }}" alt="">
                {!! $sobre->texto !!}
            </div>
            <div class="box">
                <div class="missao">
                    <h3>Missão</h3>
                    <p>{!! $sobre->missao !!}</p>
                </div>
                <div class="visao">
                    <h3>Visão</h3>
                    <p>{!! $sobre->visao !!}</p>
                </div>
                <div class="valor">
                    <h3>Valores</h3>
                    <p>{!! $sobre->valor !!}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
