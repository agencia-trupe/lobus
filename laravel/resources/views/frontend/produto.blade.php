@extends('frontend.common.template')

@section('content')

    <div class="produto-interna">
        <div class="center">
            <img class="capa" src="{{ asset('assets/img/produtos/ampliado/'.$produto->imagem) }}" alt="">
            <div class="texto">
                <a href="{{ route('home') }}" class="voltar">&laquo; voltar</a>
                {!! $produto->descricao !!}
            </div>
        </div>
    </div>

@endsection
