@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Ícones /</small> Editar Ícone</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.icones.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.icones.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
