@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Ícones /</small> Adicionar Ícone</h2>
    </legend>

    {!! Form::open(['route' => 'painel.icones.store', 'files' => true]) !!}

        @include('painel.icones.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
