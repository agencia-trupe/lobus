export default function MobileToggle() {
    var $handle = $('#mobile-toggle'),
        $nav    = $('#menu');

    $handle.on('click touchstart', function(event) {
        event.preventDefault();
        $nav.toggleClass('open');
        $handle.toggleClass('close');
    });
};
