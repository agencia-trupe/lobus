import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$('.banners').cycle({
    slides: '>.slide',
    next: '#cycle-next',
    prev: '#cycle-prev'
});

$('.servicos-slides').cycle({
    slides: '>.slide',
    next: '#cycle-next-s',
    prev: '#cycle-prev-s',
    autoHeight: 'calc',
    timeout: 0
});

if (!isNaN(getParameterByName('id'))) {
    $('.servicos-slides').cycle(parseInt(getParameterByName('id')));
}

$('.scroll-handle').click(function(e) {
    e.preventDefault();
    var hash = $(this).attr('href');

    if (hash === '#home') {
        return $('html, body').animate({
            scrollTop: 0
        }, 500);
    }

    if ($(hash).length) {
        if ($(this).hasClass('scroll-servicos')) {
            $('.servicos-slides').cycle($(this).data('id'));
        }

        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 500);
    }
});

$('.submenu-handle').click(function(e) {
    e.preventDefault();

    $(this).toggleClass('open').parent().next().slideToggle();
});

$(document).on('submit', '#form-contato', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            var error = 'Preencha todos os campos corretamente.';
            $response.fadeOut().text(error).fadeIn('slow');
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});
