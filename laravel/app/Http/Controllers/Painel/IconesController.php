<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\IconesRequest;
use App\Http\Controllers\Controller;

use App\Models\Icone;

class IconesController extends Controller
{
    private $limite = 12;

    public function index()
    {
        $registros = Icone::ordenados()->get();
        $limite    = $this->limite;

        return view('painel.icones.index', compact('registros', 'limite'));
    }

    public function create()
    {
        return view('painel.icones.create');
    }

    public function store(IconesRequest $request)
    {
        try {
            if ($this->limite <= Icone::count()) {
                throw new \Exception('Limite de registros excedido.');
            }

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Icone::upload_imagem();

            Icone::create($input);

            return redirect()->route('painel.icones.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Icone $registro)
    {
        return view('painel.icones.edit', compact('registro'));
    }

    public function update(IconesRequest $request, Icone $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Icone::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.icones.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Icone $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.icones.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
