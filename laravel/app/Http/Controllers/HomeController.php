<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Icone;
use App\Models\Produto;
use App\Models\Servico;
use App\Models\Sobre;
use App\Models\Contato;
use App\Models\ContatoRecebido;

class HomeController extends Controller
{
    public function index()
    {
        $banners  = Banner::ordenados()->get();
        $icones   = Icone::ordenados()->get();
        $produtos = Produto::ordenados()->get();
        $servicos = Servico::ordenados()->get();
        $sobre    = Sobre::first();

        return view('frontend.home', compact(
            'banners', 'icones', 'produtos', 'servicos', 'sobre'
        ));
    }

    public function produto(Produto $produto)
    {
        return view('frontend.produto', compact('produto'));
    }

    public function contato(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, 'Lobus')
                        ->subject('[CONTATO] Lobus')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
