<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 295,
            'height' => 212,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/produtos/ampliado/'
        ]);
    }
}
