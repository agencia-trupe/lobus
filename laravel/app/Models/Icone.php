<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Icone extends Model
{
    protected $table = 'icones';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'       => 160,
            'height'      => 160,
            'transparent' => true,
            'path'        => 'assets/img/icones/'
        ]);
    }
}
