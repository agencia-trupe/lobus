<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Servico extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'  => 149,
            'height' => 110,
            'path'   => 'assets/img/servicos/'
        ]);
    }
}
